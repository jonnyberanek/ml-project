d1 = readtable("votes.csv");
d2 = d1{:,:};
data = zeros(size(d2));

r = 114; % ascii value of "r"
d = 100; % ascii value of "d"

yes = 1;
no = -1;
noVote = 0;

% this turns the first column into numerical labels, 114 for republican
% 100 for democrat.  The remaining vote columns are turned from "y" to 1
% "n" to -1 and "?" to 0 and then noVotes are changed to the majority vote
for i = 1:size(data,1)
    for j = 1:size(data,2)
        if j==1
            isR = string(d2(i,j))=="republican";
            if isR data(i,j) = r; else data(i,j) = d; end
        else
            vote = string(d2(i,j));
            if vote == "y"
                data(i,j) = yes;
            elseif vote == "n"
                data(i,j) = no;
            else
                data(i,j) = noVote;
            end
        end
    end
    % find majority vote for this row and change 0's to the majority
    majorityVote = sum(data(i,2:end)); % first col is class label so skip
    noVoteCols = (data(i,:) == 0);
    if majorityVote >= 0
        data(i,noVoteCols) = 1;
    else
        data(i,noVoteCols) = -1;
    end 
end

% end preprocessing, might want to save the data matrix to a file so you 
% dont have to repeat the above code every iteration of your development
save data.mat data

% now whenever you want to start from the preprocessed data you can clear
% your workspace with:
clear
% and load your data matrix with
load data


mat = magic(10)

% example code for how to split the table into rows where the vote was yes
% for an attribute and rows where the vote was no
attributeCol = 3; % this is the second attribute, which is in col 3
yesRows = data(:,attributeCol) == 1; % colum vector, rows with 1 for yes votes 0 for no 
noRows = data(:,attributeCol) == -1; % colum vector, rows with 1 for no votes 0 for yes
yesExamples = data(yesRows,:); % select all columns of yesRows
noExamples = data(noRows,:); % select all columns of yesRows

% the above does not separate into test and train set

% matrix operations tutorial
% create a 11x11 matrix
mat = magic(11)
% choose 3 indexes in the range 1 to 11
idx1 = randi(11,1,3) % creates a row vector length 3
% grab the 3 rows of mat that are indexed by idx1;
mat3rows = mat(idx1,:)
% create a matrix with the remaining 8 rows
matTemp = mat;
matTemp(idx1,:) = [] %deletes the three rows indexed by idx1
mat8rows = matTemp  % of course this leaves some extra data in memory
% can delete with:
clear matTemp

% which rows of the original matrix have the first val greater than 50?
gt50idx = mat(:,1)>50
% if I want to know just how many rows have firest val greater than 50
numRowsGt50 = sum(gt50idx)
% if I want to pull out these rows:
gt50mat = mat(gt50idx,:)



