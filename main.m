clc;clear;close all;

load data.mat

% splits data set at size * .8 such that trainingSet is the larger and
%  testing is the smaller
split = floor(size(data,1)*.8);
mask = randperm(size(data, 1));

trainingSet = data(mask(1:split),:);
testingSet = data(mask(split+1:size(data,1)),:);

trainingFeatures = trainingSet(:,1:4);
trainingFeatures = [trainingFeatures trainingFeatures(:,1).*trainingFeatures(:,2)]; 
trainingLabels = trainingSet(:,5);

testingFeatures = testingSet(:,1:4);
testingFeatures = [testingFeatures testingFeatures(:,1).*testingFeatures(:,2)];
testingLabels = testingSet(:,5);

oldNodes = 0;
depth = 1;

depthHistory = [];
testFailureHistory = [];
trainedFailureHistory = [];

% decisionTree = DecisionTree();
% decisionTree.build(trainingSet(:,1:4), trainingSet(:,5), 3);



while 1
    % create and train tree
    decisionTree = DecisionTree();
    decisionTree.build(trainingFeatures, trainingLabels, depth)

    numNodes = decisionTree.getNumNodes();
    
    % if no more new nodes, export data and finish
    if(oldNodes >= numNodes)
       fprintf("No new splits have been made, quitting at depth of %d.\n", depth);
       results = table(depthHistory,trainedFailureHistory,testFailureHistory); 
       return
    end
    oldNodes = numNodes;
    depthHistory = [depthHistory; depth];
    
    
    fprintf("--- Depth %d ---\n", depth)
    
    % -- Code for checking and displaying error -- 
    % find training data error
    failureCount = 0;
    for i = 1:size(trainingFeatures,1)
        %fprintf("%d == %d\n", trainingLabels(i,1), decisionTree.guess(trainingFeatures(i,:)));
        if decisionTree.guess(trainingFeatures(i,:)) ~= trainingLabels(i,1)
           failureCount = failureCount + 1; 
        end
    end
    trainedFailureHistory = [trainedFailureHistory; failureCount/size(trainingSet,1)];
    fprintf("Training Data - percentage failed: %f\n", failureCount/size(trainingSet,1));
    
    % find testing data error
    failureCount = 0;
    for i = 1:size(testingSet,1)

        if decisionTree.guess(testingFeatures(i,:)) ~= testingLabels(i,1)
           failureCount = failureCount + 1; 
        end
    end
    testFailureHistory = [testFailureHistory; failureCount/size(testingSet,1)];
    fprintf("Testing Data - percentage failed: %f\n\n", failureCount/size(testingSet,1));
    
    depth = depth + 1;
    oldTree = decisionTree;
    % end error code
    
    
end