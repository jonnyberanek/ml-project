
classdef DecisionTree < handle %has to inherit from handle so it functions like an actual class should in any other language
    properties
        TreeRoot
        FeatureKeys     % a cell array of the lists of all possible values for each feature
    end
    methods
        
        function build(this, trainingFeatures, trainingLabels, maxDepth)
            featureBlacklist = [];
            this.FeatureKeys = cell(1,size(trainingFeatures, 2));
            for i = 1:size(trainingFeatures, 2)
                this.FeatureKeys{i} = transpose(unique(trainingFeatures(:,i)));
            end       
            this.TreeRoot = this.buildNode(trainingFeatures, trainingLabels, featureBlacklist, maxDepth);
        end
        
        % guesses label of dataEntry based on it's feature values
        function labelValue = guess(this, dataEntry)
            labelValue = this.nextGuessNode(this.TreeRoot, dataEntry);
        end
        
        function numNodes = getNumNodes(this)
            numNodes = this.countBranches(this.TreeRoot);
        end
       
%         % returns true if two trees are equal, false if not
%         function isEqual = equals(this, other)
%             isEqual = this.nodeEquals(this.TreeRoot, other.TreeRoot);
%         end
        
    end
    methods(Access=private)
        
        function node = buildNode(this, features, labels, featureBlacklist, depth)
            % base cases
            % 2. "when all examples sorted to a node have the same class label"
            allSame = true;
            labelValue = labels(1,1);
            for x = transpose(labels)
                if(x ~= labelValue)
                    allSame = false;
                    break
                end
            end
            if(allSame)
               node = labelValue;
               return
            end
            % 1. "maximum tree depth is reached for all possible paths"
            if(depth == 0 ) % should also check other cases from pdf
                % decide either D or R and return that as the value
                node = DecisionTree.decide(labels);
                return
            end
            % 3.  when all examples sorted to a node have the same feature values 
            %   for all remaining features (eg, all �no� votes for column 5 and all 
            %   �yes� votes for column 8)
            allFeaturesSame = true;
            for i = 1:size(features, 2)
                featureSame = true;
                featureValue = features(1,i);
                for x = transpose(features(:,i))
                    if featureValue ~= x
                       featureSame = false;
                       break
                    end
                end
                if(~featureSame)
                   allFeaturesSame = false;
                   break
                end
            end
            if(allFeaturesSame)
               node = DecisionTree.decide(labels);
               return
            end
            %end base cases
            
            maxGain = -1;
            maxFeatureIndex = 1;
            for i = 1:size(features, 2)
                if ismember(i, featureBlacklist)
                    continue
                end
                gain = DecisionTree.gain(1, transpose(labels), transpose(features(:,i)));
                if(gain > maxGain)
                    maxGain = gain;
                    maxFeatureIndex = i;
                end
            end
            featureBlacklist = [featureBlacklist, maxFeatureIndex];
            allFeatureValues = this.FeatureKeys{maxFeatureIndex};
            node = MultiNode(maxFeatureIndex);
            % set up for data subsets found associated with each child
            featureSubsets = containers.Map('KeyType','double','ValueType','any');
            labelSubsets = containers.Map('KeyType','double','ValueType','any');
            for v = allFeatureValues
               featureSubsets(v) = []; 
               labelSubsets(v) = [];
            end
            
            %filter data entries into subsets according to the value of the
            %maxFeatureIndex for entry
            for i = 1:size(features, 1)
                for x = allFeatureValues
                    if(features(i, maxFeatureIndex) == x)
                        featureSubsets(x) = [featureSubsets(x); features(i,:)];
                        labelSubsets(x) = [labelSubsets(x); labels(i, :)];
                        break
                    end
                end
            end
            % decide on the value if empty, otherwise, continue to recurse
            % with new subset created for that value
            for x = allFeatureValues
                if(isempty(featureSubsets(x)))
                    node.setChild(x, DecisionTree.decide(labels));
                else
                    node.setChild(x, buildNode(this, featureSubsets(x), labelSubsets(x), featureBlacklist, depth-1));
                end
            end
            
            
            
            
            
%             % filter in to two subsets to give each child it's respective subset
%             yesSubset = [];
%             noSubset = [];
%             for i = 1:size(features, 1)
%                 if(features(i,maxFeatureIndex) == 1)
%                     yesSubset = [yesSubset;features(i,:)];
%                 else
%                     noSubset = [noSubset;features(i,:)];
%                 end
%             end
%             %isempty checks for paths which have no more entries, decision is made
%             % based off of the current subset
%             if isempty(yesSubset) 
%                 node.YesChild = DecisionTree.decide(labels);
%             else
%                 node.YesChild = buildNode(this, yesSubset, featureBlacklist, depth-1);
%             end 
%             if isempty(noSubset) 
%                 node.NoChild = DecisionTree.decide(labels);
%             else
%                 node.NoChild = buildNode(this, noSubset, featureBlacklist, depth-1);
%             end
        end
        
        % traverses the tree based of of entry values until it hits a number
        %  once a number is found it triggers the base case and returns
        function nextNode = nextGuessNode(this, node, dataEntry)
            nextNode = node.getChild(dataEntry(node.Value));
            if ~isa(nextNode, 'double')
                nextNode = this.nextGuessNode(nextNode, dataEntry);
            end
        end
        
        function count = countBranches(this, node)
            if(~isa(node, 'MultiNode'))
               count = 1;
               return
            end
            count = 1;
            for k = node.Children.keys
               count = count + this.countBranches(node.getChild(k{1,1}));
            end
            %count = 1 + this.countBranches(node.YesChild) + this.countBranches(node.NoChild);
        end
        
    end
    methods(Access=private, Static=true)
        % expects data array
        function e = entropy(data)
            e = 0;
            % values is all possible values in the given data
            values = unique(data);
            counts = zeros(1:length(values));            
            for i = data
                for j = 1:length(counts)
                    if(i == values(j))
                        counts(j) = counts(j) + 1;
                        break
                    end
                end
            end
            for i = counts
                prob = i / length(data);
                e = e + prob * log2(prob);
            end
            e = e * -1;
        end
        
        % header is rep/dem column as array, expects data as array
        function g = gain(parentEntropy, labels, data)
            g = parentEntropy;
            values = unique(data);
            for i = 1:size(values, 1)
                mask = data == values(i); 
                g = g - (size(labels(mask),1)/size(labels,1)) * DecisionTree.entropy(labels(mask));
            end
        end
        
        % expects all remaining labels
        % returns a char
        function decision = decide(labels)
            labels = transpose(labels);
            values = unique(labels);
            counts = zeros(size(values));            
            for label = labels
                for j = 1:length(counts)
                    if(label == values(j))
                        counts(j) = counts(j) + 1;
                        break
                    end
                end
            end
            [~, i] = max(counts);
            decision = values(i);
        end
        
%         % a
%         function isEqual = nodeEquals(node1, node2)
%             if isa(node1, 'BiNode')
%                 if isa(node2, 'BiNode')
%                     yesRes = DecisionTree.nodeEquals(node1.YesChild, node2.YesChild);
%                     noRes = DecisionTree.nodeEquals(node1.NoChild, node2.NoChild);
%                     isEqual = yesRes && noRes;
%                 else
%                     isEqual = false;
%                 end
%             else
%                 if isa(node2, 'BiNode')
%                     isEqual = false; 
%                 else
%                     isEqual = node2 == node1;
%                 end
%             end
%         end
    end
    
end 


