 classdef MultiNode < handle
    properties
        Value
        Children
    end
    methods
        function obj = MultiNode(value)
            obj.Value = value;
            obj.Children = containers.Map('KeyType','double','ValueType','any');
        end
        function child = getChild(this, key)
            child = this.Children(key);
        end
        function setChild(this, key, value)
            this.Children(key) = value; 
        end
    end
end
