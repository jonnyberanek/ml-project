load data.mat

% splits data set at size * .8 such that trainingSet is the larger and
%  testing is the smaller
split = floor(size(data,1)*.8);
mask = randperm(size(data, 1));

trainingSet = data(mask(1:split),:);
testingSet = data(mask(split+1:size(data,1)),:);

oldNodes = 0;
depth = 1;

depthHistory = [];
testFailureHistory = [];
trainedFailureHistory = [];

while 1
    decisionTree = DecisionTree();
    decisionTree.build(trainingSet, depth)

    numNodes = decisionTree.getNumNodes();
    
    % if no more new nodes, export data and finish
    if(oldNodes >= numNodes)
       fprintf("No new splits have been made, quitting at depth of %d.\n", depth);
       results = table(depthHistory,trainedFailureHistory,testFailureHistory); 
       return
    end
    oldNodes = numNodes;
    depthHistory = [depthHistory; depth];
    
    fprintf("--- Depth %d ---\n", depth)
    
    %find training data error
    failureCount = 0;
    for i = 1:size(trainingSet,1)

        if decisionTree.guess(trainingSet(i,:)) ~= char(trainingSet(i,1))
           failureCount = failureCount + 1; 
        end
    end
    trainedFailureHistory = [trainedFailureHistory; failureCount/size(trainingSet,1)];
    fprintf("Training Data - percentage failed: %f\n", failureCount/size(trainingSet,1));
    
    %find testing data error
    failureCount = 0;
    for i = 1:size(testingSet,1)

        if decisionTree.guess(testingSet(i,:)) ~= char(testingSet(i,1))
           failureCount = failureCount + 1; 
        end
    end
    testFailureHistory = [testFailureHistory; failureCount/size(testingSet,1)];
    fprintf("Testing Data - percentage failed: %f\n\n", failureCount/size(testingSet,1));
    
    depth = depth + 1;
    oldTree = decisionTree;
end



