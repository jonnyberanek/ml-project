% read data
table = readtable('iris.csv');
data = zeros(size(table));

% convert classifications to numeric values
for i = 1:size(table,2)
    for j = 1:size(table,1)
        val = table{j,i};
        if(i == 5)
            val = val{1,1};
            switch(val)
                case('Iris-setosa')
                    data(j,i) = 0;  
                case('Iris-versicolor')
                    data(j,i) = 1;     
                case('Iris-virginica')
                    data(j,i) = 2;  
            end
        else
           data(j,i) = val;
        end
    end
end


% bucket continuous numbers data based off of:
    % Given Summary of Statistics:
    %                  Min  Max   Mean    SD   Class Correlation
    %    sepal length: 4.3  7.9   5.84  0.83    0.7826   
    %     sepal width: 2.0  4.4   3.05  0.43   -0.4194
    %    petal length: 1.0  6.9   3.76  1.76    0.9490  (high!)
    %     petal width: 0.1  2.5   1.20  0.76    0.9565  (high!)

% referencing statistics
stdDevs = [0.83, 0.43, 1.76, 0.76];
mins = [4.3, 2.0, 1.0, 0.1];

% only need to bucket cols 1-4, col 5 is the label
for i = 1:size(data,2)-1
    stdDev = stdDevs(i);
    min = mins(i);
    for j = 1:size(data,1)
        val = data(j,i);
        k = 1;
        % k < 20 is a failsafe that shouldnt be hit but just being wary
        while(k<20)
            if(val < min + stdDev*k)
                data(j,i) = k;
                break;
            end
            k = k + 1;
        end       
    end
end


% export data
save data.mat data;
clearvars -except data;
fprintf("Data has been saved to data.mat as 'data'\n");